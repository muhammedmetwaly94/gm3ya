$(document).ready(function () {

  // clone navigation items
  let clonedNav = $('.navbar-nav').clone()
  $('.mobile_menu').append(clonedNav)

  let clonedTop = $('.topbar .social_links, .topbar_user').clone()
  $('.mobile_menu').append(clonedTop)

  // open mobile menu
  $('.navbar-toggler').click(function (e) {
    e.preventDefault();
    $('.mobile_menu').toggleClass('open')
  });

  // owl carousel
  $('.main_slider, .second_slider').owlCarousel({
    nav: true,
    dots: false,
    items: 1,
    rtl: true,
    loop: true
  })

  // partners
  $('.partners_slider').owlCarousel({
    nav: false,
    dots: true,
    margin: 30,
    rtl: true,
    loop: true,
    responsive: {
      0: {
        items: 2
      },
      575: {
        items: 3
      },
      768: {
        items: 4
      },
      1024: {
        items: 6
      }
    }
  })
  $('.owl-prev').html('<i class="fas fa-chevron-right"></i>')
  $('.owl-next').html('<i class="fas fa-chevron-left"></i>')

  $('[data-toggle="tooltip"]').tooltip()
  // init counterUp
  $(".myCounter").counterUp({
    delay: 30,
    time: 1000
  });
});